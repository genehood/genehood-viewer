# genehood-viewer

> NuxtTS application to visualize GeneHood data

## Build Setup

``` bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start

# generate static project
$ yarn generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).

## List of features

* GeneHood viewer has three areas: 1) The navigation bar, 2) the phylogenetics area and 3) the gene neighborhood area.
* The genes are represented as arrows and are organized by the phylogenetic tree. The reference gene has a bold border.
* The phylogenetic tree shows the locus of the reference gene and the name of the organism.
    * The phylogenetic tree can be ladderized.
* By hovering the mouse over the arrows, genehood displays information about the gene in the right side of the navigation bar.
    * It displays the name of the genome, the locus of the gene and the domain architecture of PFAM and transmembrane regions as predicted by TMHMM2.
* Next to explore the data, we can click on one of the genes arrows.
* The information about the selected gene appears on the information panel in red. Hovering other genes will still display the information of the genes on the left side for comparison with the selected gene.
* We can change the color of the selected gene using the control panel on the right.
* Also, when selecting a gene, GeneHood uses the score from the control panel to greedly make a group of homologs of that gene and automatically color all the members of the group with the same color of the selected gene.
* While the gene is selected, we can move the similarity slide bar to change the threshold. We can also type a number directly. GeneHood updates the members of the homologous group in real time.
* The similarity score is `-log10(E-value)`.
* There are circumstances where a gene might be in more than one homolog groups. For that reason, we can access the tooltip of the gene with a right click. The tooltip lists all the groups and also a link to the extra information about the gene in the MiST3 database.
* Groups can be deleted altogether from the tooltip of any member.

